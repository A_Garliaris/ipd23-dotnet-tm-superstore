﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TM_SuperStore
{
    /// <summary>
    /// Interaction logic for SalesOrderWizard.xaml
    /// </summary>
    public partial class SalesOrderWizard : Window
    {
        List<Product> productOrderList = new List<Product>();
        public SalesOrderWizard()
        {
            InitializeComponent();
            SO_Wizard_comboProductList.SelectedIndex = -1;
            SO_Wizard_comboProductList.ItemsSource = Globals.ctx.Products.OrderBy(Product => Product.Name).ToList();
            SO_Wizard_comboProductList.DisplayMemberPath = "Name";

        }
        private bool ValidateFields()
        {
            List<string> errorsList = new List<string>();

            if (SO_Wizard_comboProductList.SelectedItem == null)
            {
                errorsList.Add("Item must be selected");
            }

            if (!int.TryParse(SOwiz_tbQuantity.Text, out int qty))
            {
                errorsList.Add("Quanity must be numeric value");
            }
            else
            {
                Product p = (Product)SOwiz_comboProductName.SelectedItem;
                if (qty > p.QtyInStock)
                {
                    errorsList.Add("Quantity must be less then Qty in Stock ");
                }
            }

            if (errorsList.Count == 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show(string.Join("\n", errorsList.ToList()));
                return false;
            }
        }

        private void SOwiz_comboProductName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Product item = (Product)SOwiz_comboProductName.SelectedItem;
                if (item == null) { return; }
                SOwiz_tbQuantity.Text = item.ReorderQty + "";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void SOwiz_btnAdd_Click(object sender, RoutedEventArgs e)
        {
                try
                {
                    if (!ValidateFields()) { return; }

                Product p1 = (Product)SOwiz_comboProductName.SelectedItem;
                    int qty = int.Parse(SOwiz_tbQuantity.Text);
                    SalesOrderLine so = new SalesOrderLine
                    {
                        SalesOrder = Globals.SO_CurrSalesOrder,
                        Product = p1,
                        Quantity = qty,
                        Price = p1.Price
                    };
                    Globals.ctx.SalesOrderLines.Add(so);
                    p1.QtyInStock -= qty;
                    Globals.ctx.SaveChanges();

                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Database operation failed: " + ex.Message);
                }
            }

        private void Wizard_Finish(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
         
        }
    }

}
