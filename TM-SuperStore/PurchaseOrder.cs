namespace TM_SuperStore
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PurchaseOrder
    {
        public int Id { get; set; }

        public int SupplierId { get; set; }

        public int EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public bool IsPlaced { get; set; }

        public bool IsReceived { get; set; }

        public decimal SubTotal { get; set; }

        public decimal SalesTax { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }
    }
}
