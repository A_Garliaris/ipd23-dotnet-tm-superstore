namespace TM_SuperStore
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

        public class Employee
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        public DateTime DoB { get; set; }

        [Required]
        [StringLength(20)]
        public string Phone { get; set; }

        public DateTime HireDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal HourlyRate { get; set; }
        
        [NotMapped()]
        public int Seniority { get { return (int)(((DateTime.Now - HireDate).Days) / 365.25); } }

        [Required]
        [StringLength(20)]
        public string Position { get; set; }

        public byte[] Photo { get; set; }

        [Required]
        [StringLength(20)]
        public string Department { get; set; }

       
    }

}
