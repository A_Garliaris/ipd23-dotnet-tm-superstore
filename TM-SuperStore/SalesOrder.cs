namespace TM_SuperStore
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SalesOrder
    {        
        public int Id { get; set; }

        public DateTime SalesDate { get; set; }

        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
        public decimal Subtotal { get; set; }

        public decimal SalesTax { get; set; }

        public virtual ICollection<SalesOrderLine> SalesOrderLines { get; set; }
    }
}
