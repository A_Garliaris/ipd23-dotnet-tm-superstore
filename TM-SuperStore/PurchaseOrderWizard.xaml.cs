﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TM_SuperStore
{
    /// <summary>
    /// Interaction logic for PurchaseOrderWizard.xaml
    /// </summary>
    public partial class PurchaseOrderWizard : Window
    {
        List<Product> productOrderList = new List<Product>();

        public PurchaseOrderWizard()
        {
            InitializeComponent();
            try
            {
                POwiz_comboSupplierName.ItemsSource = Globals.ctx.Suppliers.OrderBy(Supplier => Supplier.Name).ToList();
                POwiz_comboSupplierName.DisplayMemberPath = "Name";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private bool ValidateFields()
        {
            if (!int.TryParse(POwiz_tbQuantity.Text, out int qty) || qty < 1)
            {
                MessageBox.Show("Quantity must be an integer greater than zero");
                return false;
            }
            else { return true; }
        }

        private void POwiz_comboSupplierName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Supplier supplier = (Supplier)POwiz_comboSupplierName.SelectedItem;
                if (supplier == null) { return; }
                List<Product> productList = new List<Product>();
                foreach (Product item in Globals.ctx.Products)
                {
                    if (item.Supplier == supplier)
                    {
                        productList.Add(item);
                    }
                }
                POwiz_comboProductName.ItemsSource = productList.OrderBy(Product => Product.Name).ToList();
                POwiz_comboProductName.DisplayMemberPath = "Name";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void POwiz_comboProductName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Product item = (Product)POwiz_comboProductName.SelectedItem;
                if (item == null) { return; }
                POwiz_tbQuantity.Text = item.ReorderQty + "";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void POwiz_btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {              
                if (!ValidateFields() == true) { return; }
                Product item = (Product)POwiz_comboProductName.SelectedItem;
                if (item == null) { return; }
                foreach (Product p in productOrderList)
                {
                    if (p == item)
                    {
                        MessageBox.Show("Item already on order, quantity may be altered on Purchase Orders Window");
                        return;
                    }
                }
                if (item.QtyInStock > item.ReorderPoint)
                {
                    MessageBoxResult result = MessageBox.Show($"Quantity in stock, {item.QtyInStock}, is higher than the reorder point, {item.ReorderPoint}.\nAre you sure you wish to order more?", "Item in stock", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result != MessageBoxResult.Yes) { return; }
                }
                else if (item.QtyOnOrder > item.ReorderPoint)
                {
                    MessageBoxResult result = MessageBox.Show($"{item.QtyOnOrder} units are already on order.\nDo you wish to order more?", "Item on order", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result != MessageBoxResult.Yes) { return; }
                }
                productOrderList.Add(item);
                item.WizardQuantity = int.Parse(POwiz_tbQuantity.Text);
                POwiz_lvProductsList.ItemsSource = productOrderList.ToList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void Wizard_Finish(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            try
            {
                PurchaseOrder po = new PurchaseOrder
                {
                    Supplier = (Supplier)POwiz_comboSupplierName.SelectedItem,
                    Employee = Globals.currentEmp,
                    Date = DateTime.Now,
                    IsPlaced = false,
                    IsReceived = false,
                    SubTotal = 0,
                    SalesTax = 0
                };
                Globals.ctx.PurchaseOrders.Add(po);
                Globals.ctx.SaveChanges();
                foreach (Product item in productOrderList)
                {
                    PurchaseOrderLine poLine = new PurchaseOrderLine
                    {
                        PurchaseOrder = po,
                        Product = item,
                        Quantity = item.WizardQuantity,
                        Cost = item.Cost
                    };
                    Globals.ctx.PurchaseOrderLines.Add(poLine);
                }
                Globals.ctx.SaveChanges();
                Globals.currentSupplier = (Supplier)POwiz_comboSupplierName.SelectedItem;
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }
    }
}
