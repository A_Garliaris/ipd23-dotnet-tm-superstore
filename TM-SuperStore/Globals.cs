﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TM_SuperStore
{
    public class Globals
    {
        public static SuperstoreModel ctx;
        public static decimal TaxRate = .15M;
        public static decimal subtotal;
        public static decimal tax;
        public static Employee currentEmp;
        public static Supplier currentSupplier;
        public static SalesOrder SO_CurrSalesOrder;
        public static System.Windows.Controls.Image img;
    }
}
