﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TM_SuperStore
{
    public partial class MainWindow
    {
        private void PO_RefreshList()
        {
            try
            {
                PO_lblLineId.Content = "";
                PO_comboProduct.SelectedIndex = -1;
                PO_tbQuantity.Text = "";
                PO_btnUpdateLine.IsEnabled = false;
                PO_btnRemoveLine.IsEnabled = false;
                PurchaseOrder po = (PurchaseOrder)PO_comboOrderId.SelectedItem;
                if (po != null)
                {
                    Globals.subtotal = 0;
                    List<PurchaseOrderLine> poLines = new List<PurchaseOrderLine>();
                    foreach (PurchaseOrderLine poLine in Globals.ctx.PurchaseOrderLines)
                    {
                        if (poLine.PurchaseOrder == po)
                        {
                            poLines.Add(poLine);
                            Globals.subtotal += (decimal)(poLine.Cost * poLine.Quantity);
                        }
                    }
                    lvPurchaseOrderLines.ItemsSource = poLines.ToList();
                    if (po.IsPlaced == true)
                    {
                        PO_lblSubtotal.Content = po.SubTotal;
                        PO_lblTax.Content = po.SalesTax;
                        PO_lblTotal.Content = po.SubTotal + po.SalesTax;
                    }
                    PO_lblSubtotal.Content = Globals.subtotal;
                    Globals.tax = Globals.subtotal * Globals.TaxRate;
                    PO_lblTax.Content = Globals.tax;
                    PO_lblTotal.Content = Globals.subtotal + Globals.tax;
                }
                else
                {
                    PO_lblSubtotal.Content = 0;
                    PO_lblTax.Content = 0;
                    PO_lblTotal.Content = 0;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_PurchaseOrderLines_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PurchaseOrderLine poLine = (PurchaseOrderLine)lvPurchaseOrderLines.SelectedItem;
            if (lvPurchaseOrderLines.SelectedItem == null)
            {
                PO_RefreshList();
                return;
            }
            PO_lblLineId.Content = poLine.Id;
            if (PO_comboProduct.SelectedItem != poLine.Product)
            {
                PO_comboProduct.SelectedItem = poLine.Product;
            }
            PO_tbQuantity.Text = poLine.Quantity + "";
            PO_lblQtyInStock.Content = poLine.Product.QtyInStock;
            PO_lblQtyOnOrder.Content = poLine.Product.QtyOnOrder;
            PO_btnUpdateLine.IsEnabled = true;
            PO_btnRemoveLine.IsEnabled = true;
        }

        private bool PO_ValidateFields()
        {
            if (!int.TryParse(PO_tbQuantity.Text, out int qty) || qty < 1)
            {
                MessageBox.Show("Quantity must be an integer greater than zero");
                return false;
            }
            else { return true; }
        }

        private void PO_LineAdd_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PO_comboOrderId.SelectedItem == null)
                {
                    MessageBox.Show("Please select or create a purchase order");
                    return;
                }
                if (!PO_ValidateFields()) { return; }
                if (PO_IsDuplicate()) { return; }
                Product item = (Product)PO_comboProduct.SelectedItem;
                PurchaseOrderLine poLine = new PurchaseOrderLine
                {
                    PurchaseOrder = (PurchaseOrder)PO_comboOrderId.SelectedItem,
                    Product = item,
                    Quantity = int.Parse(PO_tbQuantity.Text),
                    Cost = item.Cost
                };
                Globals.ctx.PurchaseOrderLines.Add(poLine);
                Globals.ctx.SaveChanges();
                PO_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_LineUpdate_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                PurchaseOrderLine poLine = (PurchaseOrderLine)lvPurchaseOrderLines.SelectedItem;
                if (poLine == null) { return; }
                if (!PO_ValidateFields()) { return; }
                Product item = (Product)PO_comboProduct.SelectedItem;
                poLine.Product = item;
                poLine.Quantity = int.Parse(PO_tbQuantity.Text);
                poLine.Cost = item.Cost;
                Globals.ctx.SaveChanges();
                PO_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_LineRemove_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                PurchaseOrderLine poLine = (PurchaseOrderLine)lvPurchaseOrderLines.SelectedItem;
                if (poLine == null) { return; }
                MessageBoxResult result = MessageBox.Show("Sure to delete record", "Delete record", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result != MessageBoxResult.Yes) { return; }
                Globals.ctx.PurchaseOrderLines.Remove(poLine);
                Globals.ctx.SaveChanges();
                PO_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_ResetFields()
        {
            PO_comboOrderId.SelectedIndex = -1;
            PO_comboSupplierName.SelectedIndex = -1;
            PO_comboOrderId.ItemsSource = Globals.ctx.PurchaseOrders.ToList();
            lvPurchaseOrderLines.ItemsSource = null;
            PO_lblSubtotal.Content = "";
            PO_lblTax.Content = "";
            PO_lblTotal.Content = "";
        }

        private void PO_PlaceOrder_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                PurchaseOrder po = (PurchaseOrder)PO_comboOrderId.SelectedItem;
                if (po == null) { return; }
                PO_RefreshList();
                MessageBoxResult result = MessageBox.Show("Are you sure you want to place this order?\nOnce placed, the order cannot be reversed.",
                    "Place Order", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result != MessageBoxResult.Yes) { return; }
                if (!DateTime.TryParse(PO_dpDatePlaced.SelectedDate.ToString(), out DateTime orderDate)) { po.Date = DateTime.Now; }
                else { po.Date = orderDate; }
                po.IsPlaced = true;
                po.SubTotal = Globals.subtotal;
                po.SalesTax = Globals.tax;
                po.Employee = Globals.currentEmp;
                foreach (PurchaseOrderLine poLine in Globals.ctx.PurchaseOrderLines)
                {
                    if (poLine.PurchaseOrder == po)
                    {
                        poLine.Product.QtyOnOrder += poLine.Quantity;
                    }
                }
                Globals.ctx.SaveChanges();
                MessageBox.Show("Order placed.");
                PO_ResetFields();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_CancelOrder_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                PurchaseOrder po = (PurchaseOrder)PO_comboOrderId.SelectedItem;
                if (po == null) { return; }
                if (po.IsPlaced == true) { return; }
                MessageBoxResult result = MessageBox.Show("Are you sure you want to cancel this order?",
                    "Cancel Order", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result != MessageBoxResult.Yes) { return; }
                foreach (PurchaseOrderLine poLine in Globals.ctx.PurchaseOrderLines)
                {
                    if (poLine.PurchaseOrder == po)
                    {
                        Globals.ctx.PurchaseOrderLines.Remove(poLine);
                    }
                }
                Globals.ctx.SaveChanges();
                PO_ResetFields();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_ReceiveOrder_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                PurchaseOrder po = (PurchaseOrder)PO_comboOrderId.SelectedItem;
                if (po == null || po.IsPlaced == false || po.IsReceived == true) { return; }
                MessageBoxResult result = MessageBox.Show("Are you sure you want to receive this order?\nOnce received, the operation cannnot be reversed",
                    "Receive Order", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result != MessageBoxResult.Yes) { return; }
                foreach (PurchaseOrderLine poLine in Globals.ctx.PurchaseOrderLines)
                {
                    if (poLine.PurchaseOrder == po)
                    {
                        poLine.Product.QtyOnOrder -= poLine.Quantity;
                        poLine.Product.QtyInStock += poLine.Quantity;
                    }
                }
                po.IsReceived = true;
                MessageBox.Show("Order received");
                PO_ResetFields();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_UpdatePO()
        {
            try
            {
                List<PurchaseOrder> poList = new List<PurchaseOrder>();
                foreach (PurchaseOrder p in Globals.ctx.PurchaseOrders)
                {
                    if (p.Supplier == PO_comboSupplierName.SelectedItem)
                    {
                        poList.Add(p);
                    }
                }
                PO_comboOrderId.ItemsSource = poList.ToList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_POId_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PO_comboOrderId.SelectedItem == null)
            {
                lvPurchaseOrderLines.ItemsSource = null;
                return;
            }
            PurchaseOrder po = (PurchaseOrder)PO_comboOrderId.SelectedItem;
            lblSupplierId.Content = po.Supplier.Id;
            PO_comboSupplierName.SelectedItem = po.Supplier;
            PO_RefreshList();
            if (po.IsPlaced == true)
            {
                PO_btnAddLine.IsEnabled = false;
                PO_btnUpdateLine.IsEnabled = false;
                PO_btnRemoveLine.IsEnabled = false;
                PO_btnPlaceOrder.IsEnabled = false;
                PO_btnCancelOrder.IsEnabled = false;
                PO_cbOrderPlaced.IsChecked = true;
                PO_dpDatePlaced.SelectedDate = po.Date;
                PO_dpDatePlaced.IsEnabled = false;
                lvPurchaseOrderLines.IsEnabled = false;
            }
            else
            {
                PO_btnAddLine.IsEnabled = true;
                PO_btnUpdateLine.IsEnabled = true;
                PO_btnRemoveLine.IsEnabled = true;
                PO_btnPlaceOrder.IsEnabled = true;
                PO_btnCancelOrder.IsEnabled = true;
                PO_cbOrderPlaced.IsChecked = false;
                PO_dpDatePlaced.SelectedDate = DateTime.Now;
                PO_dpDatePlaced.IsEnabled = true;
                lvPurchaseOrderLines.IsEnabled = true;
            }
            if (po.IsReceived == true) { PO_cbOrderReceived.IsChecked = true; }
            else { PO_cbOrderReceived.IsChecked = false; }
        }

        private void PO_UpdateProductsList()
        {
            try
            {
                List<Product> products = new List<Product>();
                foreach (Product p in Globals.ctx.Products)
                {
                    if (p.Supplier == PO_comboSupplierName.SelectedItem)
                        products.Add(p);
                }
                PO_comboProduct.ItemsSource = products.OrderBy(Product => Product.Name).ToList();
                PO_comboProduct.DisplayMemberPath = "Name";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void SupplierName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (PO_comboSupplierName.SelectedItem == null)
                {
                    lvPurchaseOrderLines.ItemsSource = null;
                    return;
                }
                PO_UpdatePO();
                PO_UpdateProductsList();
                PO_comboOrderId.SelectedIndex = PO_comboOrderId.Items.Count - 1;
                PO_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_NewPurchaseOrder_btnClick(object sender, RoutedEventArgs e)
        {
            try            
            {
                if (Globals.currentEmp == null)
                {
                    MessageBox.Show("Must be logged in as an Employee to create a Purchase Order");
                    return;
                }
                PurchaseOrderWizard wiz = new PurchaseOrderWizard() { Owner = this };
                wiz.ShowDialog();
                PO_UpdatePO();
                PO_comboSupplierName.SelectedItem = Globals.currentSupplier;
                PO_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void PO_Item_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Product item = (Product)PO_comboProduct.SelectedItem;
                if (item == null) { return; }
                PO_lblQtyInStock.Content = item.QtyInStock;
                PO_lblQtyOnOrder.Content = item.QtyOnOrder;
                PO_tbQuantity.Text = item.ReorderQty + "";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private bool PO_IsDuplicate()
        {
            try
            {
                PurchaseOrder po = (PurchaseOrder)PO_comboOrderId.SelectedItem;
                Product item = (Product)PO_comboProduct.SelectedItem;
                if (item == null) { return false; }
                foreach (PurchaseOrderLine poLine in Globals.ctx.PurchaseOrderLines)
                {
                    if (poLine.PurchaseOrder == po && poLine.Product == item)
                    {
                        lvPurchaseOrderLines.SelectedItem = poLine;
                        MessageBox.Show("Item is already on this order.\nUse update to change quantity",
                            "Duplicate item", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return true;
                    }
                }                
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
            return false;
        }
    }
}
