﻿
using CsvHelper.TypeConversion;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CsvHelper;


namespace TM_SuperStore
{
    public partial class MainWindow
    {
        private void SO_RefreshList()
        {
            try
            {
                List<SalesOrderLine> salesOrderLines = new List<SalesOrderLine>();
                Globals.subtotal = 0;
                Globals.tax = 0;
                if (Globals.SO_CurrSalesOrder != null)
                    foreach (SalesOrderLine s in Globals.ctx.SalesOrderLines)
                    {
                        if (s.SalesOrder == Globals.SO_CurrSalesOrder)

                        {
                            salesOrderLines.Add(s);
                            Globals.subtotal = Globals.subtotal + s.TotalPrice;
                        }
                    }
                Globals.tax = Globals.subtotal * Globals.TaxRate;
                SO_lblSubTotal.Content = Globals.subtotal;
                SO_lblTax.Content = Globals.tax;
                SO_lblTotal.Content = Globals.subtotal + Globals.tax;
                SO_lvSalesOrderList.Items.Refresh();
                SO_lvSalesOrderList.SelectedIndex = -1;
                SO_btnUpdateItem.IsEnabled = false;
                SO_btnRemoveItem.IsEnabled = false;
                SO_tbQuantity.Text = "";
                SO_comboProductId.SelectedIndex = -1;
                SO_lblQtyinStock.Content = "";
                SO_lvSalesOrderList.ItemsSource = salesOrderLines.ToList();

            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private bool SO_ValidateFields()
        {
            List<string> errorsList = new List<string>();

            if (SO_comboProductId.SelectedItem == null)
            {
                errorsList.Add("Item must be selected");
            }

            if (!int.TryParse(SO_tbQuantity.Text, out int qty))
            {
                errorsList.Add("Quanity must be numeric value");
            }
            else
            {
                Product p = (Product)SO_comboProductId.SelectedItem;
                if (qty > p.QtyInStock)
                {
                    errorsList.Add("Quantity must be less then Qty in Stock ");
                }
            }

            if (errorsList.Count == 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show(string.Join("\n", errorsList.ToList()));
                return false;
            }
        }

        private void SO_btnCreateSalesOrder_Click(object sender, RoutedEventArgs e)
        {
            var wiz = new SalesOrderWizard() { Owner = this };
            wiz.ShowDialog();

            if (Globals.SO_CurrSalesOrder == null)
            {
                SO_CreateOrder(null, null);  // Create a dummy order first
            }
        }

        private void SO_btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!SO_ValidateFields()) { return; }
                if (Globals.SO_CurrSalesOrder == null)
                {
                    SO_CreateOrder(null, null);  // Create a dummy order first
                }

                Product p1 = (Product)SO_comboProductId.SelectedItem;
                int qty = int.Parse(SO_tbQuantity.Text);
                SalesOrderLine so = new SalesOrderLine
                {
                    SalesOrder = Globals.SO_CurrSalesOrder,
                    Product = p1,
                    Quantity = qty,
                    Price = p1.Price

                };
                Globals.ctx.SalesOrderLines.Add(so);
                p1.QtyInStock -= qty;
                Globals.ctx.SaveChanges();
                SO_lblQtyinStock.Content = p1.QtyInStock;
                SO_RefreshList();

            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }


        private void SO_btnRemoveItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SalesOrderLine s1 = (SalesOrderLine)SO_lvSalesOrderList.SelectedItem;
                Product p1 = (Product)SO_comboProductId.SelectedItem;

                int qty = int.Parse(SO_tbQuantity.Text);

                if (s1 != null)
                {
                    if (p1 != null)
                    {
                        p1.QtyInStock += qty;
                    }
                    Globals.ctx.SalesOrderLines.Remove(s1);
                    Globals.ctx.SaveChanges();
                    SO_lblQtyinStock.Content = p1.QtyInStock;
                    SO_RefreshList();
                }
                else
                {
                    MessageBox.Show("Unable to find record to delete");
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void SO_btnUpdateItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!SO_ValidateFields()) { return; }

                Product p1 = (Product)SO_comboProductId.SelectedItem;
                int qty = int.Parse(SO_tbQuantity.Text);
                SalesOrderLine so = (SalesOrderLine)SO_lvSalesOrderList.SelectedItem;

                p1.QtyInStock = p1.QtyInStock + so.Quantity - qty;
                so.Quantity = qty;
                Globals.ctx.SaveChanges();
                SO_lblQtyinStock.Content = p1.QtyInStock;
                SO_RefreshList();

            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void SO_CreateOrder(object sender, RoutedEventArgs e)
        {
            SO_lblSalesOrderDate.Content = DateTime.Now;
            Globals.SO_CurrSalesOrder = new SalesOrder
            {
                SalesDate = DateTime.Now,
                Employee = Globals.currentEmp,
                Subtotal = 0,
                SalesTax = 0
            };
            Globals.ctx.SalesOrders.Add(Globals.SO_CurrSalesOrder);
            Globals.ctx.SaveChanges();
            SO_lblSalesOrderNumber.Content = Globals.SO_CurrSalesOrder.Id;
        }

        private void SO_lvSalesOrderList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (SO_lvSalesOrderList.SelectedIndex == -1)
                {
                    SO_RefreshList();
                    return;
                }
                SalesOrderLine s = (SalesOrderLine)SO_lvSalesOrderList.SelectedItem;

                SO_comboProductId.SelectedItem = s.Product;

                SO_tbQuantity.Text = s.Quantity + "";
                SO_btnUpdateItem.IsEnabled = true;
                SO_btnRemoveItem.IsEnabled = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void SO_btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SalesOrder so = Globals.SO_CurrSalesOrder;
                if (so == null) { return; }
                MessageBoxResult result = MessageBox.Show("Are you sure you want to cancel this order?",
                    "Cancel Order", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result != MessageBoxResult.Yes) { return; }


                foreach (SalesOrderLine soLine in Globals.ctx.SalesOrderLines)
                {
                    if (soLine.SalesOrder == so)
                    {
                        Product p1 = soLine.Product;
                        p1.QtyInStock += soLine.Quantity;
                        Globals.ctx.SalesOrderLines.Remove(soLine);
                    }
                }
                Globals.ctx.SaveChanges();    /// should I update this inside the loop????

                Globals.ctx.SalesOrders.Remove(so);
                Globals.ctx.SaveChanges();
                SO_ResetFields();
                SO_lblSalesOrderNumber.Content = "";

            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void SO_comboProductId_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Product p = (Product)SO_comboProductId.SelectedItem;
            if (p == null) { return; }
            SO_lblQtyinStock.Content = p.QtyInStock;
            SO_tbQuantity.Text = "";
        }

        private void SO_btnProcessOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SalesOrder so = Globals.SO_CurrSalesOrder;
                if (so == null) { return; }

                MessageBoxResult result = MessageBox.Show("Are you sure you want to Process this Order?\nOnce Ordered, the operation cannnot be reversed",
                    "Process Order", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result != MessageBoxResult.Yes) { return; }

                SaveOrderToFile();

                MessageBox.Show("Order Processed");


                SO_ResetFields();
                //SO_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        private void SaveOrderToFile()
        {
            string fileName = Globals.SO_CurrSalesOrder + ".csv";
                try
                {
                    using (StreamWriter writer = new StreamWriter(fileName))
                    using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                       
                        List<SalesOrderLine> list = new List<SalesOrderLine>();
                        foreach (SalesOrderLine so1 in SO_lvSalesOrderList.Items)
                        {
                            list.Add(so1);
                        }
                        csv.WriteRecords(list);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error exporting to csv: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
          
        }
       
        private void SO_ResetFields()
        {
            SO_lblSalesOrderNumber.Content = "";
            SO_comboProductId.SelectedIndex = -1;
            SO_lblQtyinStock.Content = "";
            SO_lvSalesOrderList.ItemsSource = null;
            SO_lblSubTotal.Content = "";
            SO_lblTax.Content = "";
            SO_lblTotal.Content = "";

        }

    }
}
