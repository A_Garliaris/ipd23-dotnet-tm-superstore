﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TM_SuperStore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
		
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                // Code for Employee Tab
                EM_RefreshList();

                // Code for Products Tab
                PR_RefreshSuppliers();
                PR_RefreshList();

                // Code for Suppliers Tab
                SU_RefreshList();

                // Code for Purchase Order Tab
                PO_ResetFields();
                PO_lblEmployeeId.Content = Globals.currentEmp.Id;
                PO_lblEmployeeName.Content = Globals.currentEmp.Name;
                PO_comboOrderId.ItemsSource = Globals.ctx.PurchaseOrders.OrderBy(PurchaseOrder => PurchaseOrder.Id).ToList();
                PO_comboOrderId.DisplayMemberPath = "Id";
                PO_comboOrderId.SelectedItem = null;
                PO_comboSupplierName.ItemsSource = Globals.ctx.Suppliers.OrderBy(Supplier => Supplier.Name).ToList();
                PO_comboSupplierName.DisplayMemberPath = "Name";
                PO_comboSupplierName.SelectedItem = null;
                PO_dpDatePlaced.SelectedDate = DateTime.Now;
                

                // Code for Sales Order Tab
                SO_ResetFields();
                SO_lblEmployeeName.Content = Globals.currentEmp.Name;
                SO_lblSalesOrderDate.Content = DateTime.Now;
                SO_comboProductId.SelectedIndex = -1;
                SO_comboProductId.ItemsSource = Globals.ctx.Products.OrderBy(Product => Product.Name).ToList();
                SO_comboProductId.DisplayMemberPath = "Name";
                SO_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database Operation Failed: " + ex.Message);                
            }
        }

        
        

        

        

        
    }
}
