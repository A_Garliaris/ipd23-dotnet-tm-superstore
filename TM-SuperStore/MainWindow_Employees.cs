﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TM_SuperStore
{
    public partial class MainWindow
    {
        private void EM_RefreshList()
        {
            EM_btnUpdateEmployee.IsEnabled = false;
            EM_btnDeleteEmployee.IsEnabled = false;
            EM_lvEmployeeList.SelectedItem = -1;
            EM_lvEmployeeList.ItemsSource = Globals.ctx.Employees.ToList();
            EM_lvEmployeeList.SelectedIndex = -1;
            EM_lvEmployeeList.Items.Refresh();
        }
        private bool EM_ValidateFields()
        {
            List<string> errorsList = new List<string>();

            if (Globals.img == null)
            {
                errorsList.Add("Please select picture for the employee.");
            }
            if (EM_tbName.Text.Length < 2 || EM_tbName.Text.Length > 50)
            {
                errorsList.Add("Name length should be 2 to 50 characters long.");
            }
            if (EM_tbAddress.Text.Length < 2 || EM_tbName.Text.Length > 50)
            {
                errorsList.Add("Address should be 2 to 50 characters long.");
            }

            if (EM_dpDob.SelectedDate == null)
            {
                errorsList.Add("DOB Can not be null");
            }
            if (EM_dpHireDate.SelectedDate == null)
            {
                errorsList.Add("HireDate can not be null.");
            }
            if (!double.TryParse(EM_tbHrRate.Text, out double rate) || rate < 0)
            {
                errorsList.Add("Hourly Rate must be a positive number.");
            }
            if (errorsList.Count == 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show(string.Join("\n", errorsList.ToList()));
                return false;
            }
        }
        private void EM_btnAddEmployee_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!EM_ValidateFields()) { return; }
                Employee e1 = new Employee
                {
                    Name = EM_tbName.Text,
                    Address = EM_tbAddress.Text,
                    DoB = (DateTime)EM_dpDob.SelectedDate,
                    Phone = EM_tbPhone.Text,
                    HourlyRate = (decimal)double.Parse(EM_tbHrRate.Text),
                    Position = EM_tbPosition.Text,

                    HireDate = (DateTime)EM_dpHireDate.SelectedDate,
                    Department = EM_tbDept.Text,
                    Photo = EM_ImageToByteArray(Globals.img),
                };
                Globals.ctx.Employees.Add(e1);
                Globals.ctx.SaveChanges();
                EM_RefreshList();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed Could not add/update", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EM_btnUpdateEmployee_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!EM_ValidateFields()) { return; }

                Employee e2 = (Employee)EM_lvEmployeeList.SelectedItem;
                if (e2 == null) { return; }
                e2.Name = EM_tbName.Text;
                e2.Address = EM_tbAddress.Text;
                e2.DoB = (DateTime)EM_dpDob.SelectedDate;
                e2.HourlyRate = (decimal)double.Parse(EM_tbHrRate.Text);
                e2.Position = EM_tbPosition.Text;
                e2.Phone = EM_tbPhone.Text;
                e2.HireDate = (DateTime)EM_dpHireDate.SelectedDate;
                if (EM_dpEndDate.SelectedDate != null) { e2.EndDate = (DateTime)EM_dpEndDate.SelectedDate; }
                e2.Department = EM_tbDept.Text;
                e2.Photo = EM_ImageToByteArray(Globals.img);
                Globals.ctx.SaveChanges();
                EM_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }
        private void EM_btnDeleteEmployee_Click(object sender, RoutedEventArgs e)
        {
            Employee e3 = (Employee)EM_lvEmployeeList.SelectedItem;
            if (e3 == null) { return; }
            Globals.ctx.Employees.Remove(e3);
            Globals.ctx.SaveChanges();
            EM_RefreshList();
        }
        public void EM_ClearInputs()
        {
            EM_tbName.Text = "";
            EM_tbAddress.Text = "";
            EM_tbDept.Text = "";
            EM_tbHrRate.Text = "";
            EM_tbPhone.Text = "";
            EM_tbPosition.Text = "";
            EM_btnEmployeePhoto.Content = "Load Employee Picture";
            EM_btnUpdateEmployee.IsEnabled = false;
            EM_btnDeleteEmployee.IsEnabled = false;
        }
        private void lvEmployeeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (EM_lvEmployeeList.SelectedIndex == -1)
                {
                    EM_RefreshList();
                    return;
                }
                {
                    Employee e4 = (Employee)EM_lvEmployeeList.SelectedItem;

                    EM_tbName.Text = e4.Name;
                    EM_tbAddress.Text = e4.Address;
                    EM_tbPosition.Text = e4.Position;
                    EM_dpHireDate.SelectedDate = e4.HireDate;
                    EM_dpDob.SelectedDate = e4.DoB;
                    EM_dpEndDate.SelectedDate = e4.EndDate;
                    EM_tbPhone.Text = e4.Phone;
                    EM_tbDept.Text = e4.Department;
                    EM_tbHrRate.Text = e4.HourlyRate + "";
                    EM_btnEmployeePhoto.Content = imgEmployeePhoto;
                    imgEmployeePhoto.Source = EM_ByteArrayToBitmapImage(e4.Photo);
                    Globals.img = imgEmployeePhoto;
                    EM_btnUpdateEmployee.IsEnabled = true;
                    EM_btnDeleteEmployee.IsEnabled = true;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void EM_BtnEmployeePhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                Uri fileUri = new Uri(openFileDialog.FileName);
                Globals.img = new System.Windows.Controls.Image();
                EM_btnEmployeePhoto.Content = Globals.img;
                Globals.img.Stretch = Stretch.Uniform;
                Globals.img.Source = new BitmapImage(fileUri);
            }
        }
        public static BitmapImage EM_ByteArrayToBitmapImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; // here
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }

        public static BitmapImage EM_ConvertByteArrayToBitmapImage(Byte[] bytes)
        {
            var stream = new MemoryStream(bytes);
            stream.Seek(0, SeekOrigin.Begin);
            var image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }

        public byte[] EM_ImageToByteArray(System.Windows.Controls.Image img)
        {
            MemoryStream ms = new MemoryStream();
            var bmp = img.Source as BitmapImage;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmp));
            encoder.Save(ms);
            return ms.ToArray();
        }
    }
}
