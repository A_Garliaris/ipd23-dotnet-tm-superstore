﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TM_SuperStore
{
    public partial class MainWindow 
    {
        private void PR_RefreshList()
        {
            try
            {
                PR_lblProductId.Content = "";
                PR_tbProductName.Text = "";
                PR_comboProductSupplierName.SelectedIndex = -1;
                PR_tbProductCategory.Text = "";
                PR_tbProductCost.Text = "";
                PR_tbProductSalesMargin.Text = "";
                PR_lblProductPrice.Content = "";
                PR_lblProductQtyInStock.Content = "";
                PR_lblProductQtyOnOrder.Content = "";
                PR_tbProductReorderPoint.Text = "";
                PR_tbProductReorderQty.Text = "";
                PR_btnProductUpdate.IsEnabled = false;
                PR_btnProductRemove.IsEnabled = false;
                lvProductsList.SelectedIndex = -1;
                lvProductsList.ItemsSource = Globals.ctx.Products.Include("Supplier").ToList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void PR_RefreshSuppliers()
        {
            try
            {
                PR_comboProductSupplierName.ItemsSource = Globals.ctx.Suppliers.OrderBy(Supplier => Supplier.Name).ToList();
                PR_comboProductSupplierName.DisplayMemberPath = "Name";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private bool PR_ValidateFields()
        {
            List<string> errorsList = new List<string>();
            if (PR_tbProductName.Text.Length < 2 || PR_tbProductName.Text.Length > 50)
            {
                errorsList.Add("Item length should be 2 to 50 characters long.");
            }
            if (PR_comboProductSupplierName.SelectedItem == null)
            {
                errorsList.Add("Supplier name must be selected.");
            }
            if (PR_tbProductCategory.Text.Length < 2 || PR_tbProductCategory.Text.Length > 20)
            {
                errorsList.Add("Category must be 2 to 20 characters long.");
            }
            if (!double.TryParse(PR_tbProductCost.Text, out double cost) || cost < 0)
            {
                errorsList.Add("Cost must be a positive number.");
            }
            if (!double.TryParse(PR_tbProductSalesMargin.Text, out double salesMargin) || salesMargin < 0)
            {
                errorsList.Add("Sales margin must be a positive number.");
            }
            if (!int.TryParse(PR_tbProductReorderPoint.Text, out int reorderPoint) || reorderPoint < 0)
            {
                errorsList.Add("Reorder point must be an integer number.");
            }
            if (!int.TryParse(PR_tbProductReorderQty.Text, out int reorderQty) || reorderQty < 0)
            {
                errorsList.Add("Reorder quantity must be an integer number.");
            }
            if (errorsList.Count == 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show(string.Join("\n", errorsList.ToList()));
                return false;
            }
        }

        private void PR_Add_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!PR_ValidateFields()) { return; }
                Product p = new Product
                {
                    Name = PR_tbProductName.Text,
                    Supplier = (Supplier)PR_comboProductSupplierName.SelectedItem,
                    Category = PR_tbProductCategory.Text,
                    Cost = (decimal)double.Parse(PR_tbProductCost.Text),
                    SalesMargin = (decimal)double.Parse(PR_tbProductSalesMargin.Text),
                    QtyInStock = 0,
                    QtyOnOrder = 0,
                    ReorderPoint = int.Parse(PR_tbProductReorderPoint.Text),
                    ReorderQty = int.Parse(PR_tbProductReorderQty.Text)
                };
                Globals.ctx.Products.Add(p);
                Globals.ctx.SaveChanges();
                PR_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void PR_Update_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!PR_ValidateFields()) { return; }
                Product p = (Product)lvProductsList.SelectedItem;
                p.Name = PR_tbProductName.Text;
                p.Supplier = (Supplier)PR_comboProductSupplierName.SelectedItem;
                p.Category = PR_tbProductCategory.Text;
                p.Cost = (decimal)double.Parse(PR_tbProductCost.Text);
                p.SalesMargin = (decimal)double.Parse(PR_tbProductSalesMargin.Text);
                p.ReorderPoint = int.Parse(PR_tbProductReorderPoint.Text);
                p.ReorderQty = int.Parse(PR_tbProductReorderQty.Text);
                Globals.ctx.SaveChanges();
                PR_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void PR_Remove_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Sure to remove Product?", "Remove Product", MessageBoxButton.YesNo,
                    MessageBoxImage.Question) != MessageBoxResult.Yes) { return; }
                Product p = (Product)lvProductsList.SelectedItem;
                if (p == null) { return; }
                Globals.ctx.Products.Remove(p);
                Globals.ctx.SaveChanges();
                PR_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void PR_ProductsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (lvProductsList.SelectedIndex == -1)
                {
                    PR_RefreshList();
                    return;
                }
                Product p = (Product)lvProductsList.SelectedItem;
                PR_lblProductId.Content = p.Id;
                PR_tbProductName.Text = p.Name;
                PR_comboProductSupplierName.SelectedItem = p.Supplier;
                PR_tbProductCategory.Text = p.Category;
                PR_tbProductCost.Text = p.Cost.ToString("0.00");
                PR_tbProductSalesMargin.Text = p.SalesMargin.ToString("0.00");
                PR_lblProductPrice.Content = p.Price.ToString("0.00");
                PR_lblProductQtyInStock.Content = p.QtyInStock + "";
                PR_lblProductQtyOnOrder.Content = p.QtyOnOrder + "";
                PR_tbProductReorderPoint.Text = p.ReorderPoint + "";
                PR_tbProductReorderQty.Text = p.ReorderQty + "";
                PR_btnProductUpdate.IsEnabled = true;
                PR_btnProductRemove.IsEnabled = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }
    }
}
