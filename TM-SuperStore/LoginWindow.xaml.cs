﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TM_SuperStore
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        string currentName;
        
        public LoginWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new SuperstoreModel();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database Operation Failed: " + ex.Message);
                Environment.Exit(1);
            }            
        }
        private bool ValidateEmployee()
        {
            foreach (Employee emp in Globals.ctx.Employees)
            {
                if (emp.Name == currentName)
                {
                    Globals.currentEmp = emp;
                    return true;
                }
            }
            MessageBox.Show("Employee name or password is incorrect.", "Employee Verification", MessageBoxButton.OK, MessageBoxImage.Error);
            return false;
        }

            
        private void Login_btnClick(object sender, RoutedEventArgs e)
        {
            currentName = tbLoginName.Text;
            if (currentName != "admin")
            {
                if (!ValidateEmployee()) { return; }                
            }
            else
            {
                Globals.currentEmp = null;
            }

            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}
