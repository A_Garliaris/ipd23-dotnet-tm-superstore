namespace TM_SuperStore
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

public class Product
    {        
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Category { get; set; }

        public decimal Cost { get; set; }

        public int QtyInStock { get; set; }

        public int QtyOnOrder { get; set; }

        public int ReorderPoint { get; set; }

        public int ReorderQty { get; set; }
        
        public decimal SalesMargin { get; set; }

        public int SupplierId { get; set; }

        public virtual Supplier Supplier { get; set; }

        [NotMapped]
        public decimal Price { get { return Cost * (1 + SalesMargin / 100); } }

        [NotMapped]
        public int WizardQuantity { get; set; }

        public virtual ICollection<SalesOrderLine> SalesOrderLines { get; set; }

        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }
    }
}
