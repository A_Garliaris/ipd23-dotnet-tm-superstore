﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TM_SuperStore
{
    public partial class MainWindow
    {
        private void SU_RefreshList()
        {
            try
            {
                SU_btnUpdate.IsEnabled = false;
                SU_btnRemove.IsEnabled = false;
                lvSupplierList.SelectedIndex = -1;
                lvSupplierList.ItemsSource = Globals.ctx.Suppliers.ToList();
                SU_lblSupplierId.Content = "";
                SU_tbSupplierName.Text = "";
                SU_tbSupplierAddress.Text = "";
                SU_tbSupplierContact.Text = "";

            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private bool ValidateFields()
        {
            List<string> errorsList = new List<string>();
            if (SU_tbSupplierName.Text.Length < 2 || SU_tbSupplierName.CaretIndex > 50)
            {
                errorsList.Add("Supplier name must be between 2 and 50 characters long.");
            }
            if (SU_tbSupplierAddress.Text.Length < 2 || SU_tbSupplierAddress.Text.Length > 50)
            {
                errorsList.Add("Supplier address must be between 2 and 50 characters long.");
            }
            if (SU_tbSupplierContact.Text.Length < 2 || SU_tbSupplierContact.Text.Length > 50)
            {
                errorsList.Add("Supplier contact must be between 2 and 50 chaaraacters long.");
            }
            if (errorsList.Count == 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show(string.Join("\n", errorsList.ToList()));
                return false;
            }
        }

        private void SU_SupplierList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (lvSupplierList.SelectedItem == null)
                {
                    SU_RefreshList();
                    return;
                }
                Supplier s = (Supplier)lvSupplierList.SelectedItem;
                SU_lblSupplierId.Content = s.Id;
                SU_tbSupplierName.Text = s.Name;
                SU_tbSupplierAddress.Text = s.Address;
                SU_tbSupplierContact.Text = s.Contact;
                SU_btnUpdate.IsEnabled = true;
                SU_btnRemove.IsEnabled = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void SU_Add_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ValidateFields()) { return; }
                Supplier s = new Supplier
                {
                    Name = SU_tbSupplierName.Text,
                    Address = SU_tbSupplierAddress.Text,
                    Contact = SU_tbSupplierContact.Text
                };
                Globals.ctx.Suppliers.Add(s);
                Globals.ctx.SaveChanges();
                SU_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void SU_Update_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ValidateFields()) { return; }
                Supplier s = (Supplier)lvSupplierList.SelectedItem;
                s.Name = SU_tbSupplierName.Text;
                s.Address = SU_tbSupplierAddress.Text;
                s.Contact = SU_tbSupplierContact.Text;
                Globals.ctx.SaveChanges();
                SU_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void SU_Remove_btnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are you sure you want to remove the supplier?", "Remove Supplier", MessageBoxButton.YesNo,
                    MessageBoxImage.Question) != MessageBoxResult.Yes) { return; }
                Supplier s = (Supplier)lvSupplierList.SelectedItem;
                Globals.ctx.Suppliers.Remove(s);
                Globals.ctx.SaveChanges();
                SU_RefreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }
    }
}
